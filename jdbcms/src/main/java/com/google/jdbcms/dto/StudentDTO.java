package com.google.jdbcms.dto;

public class StudentDTO {
	
	private int id;
	
	private String firstName;
	
	private String email;
	
	private String pan;
	
	private int age;
	
	public StudentDTO(){
		
	}
	
	public StudentDTO(int id, String firstName, String email, String pan, int age) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.email = email;
		this.pan = pan;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
