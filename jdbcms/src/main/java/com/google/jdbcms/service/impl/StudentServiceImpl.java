package com.google.jdbcms.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Service;

import com.google.jdbcms.dto.StudentDTO;
import com.google.jdbcms.service.StudentService;

@Service (value = "studentServiceImpl")
public class StudentServiceImpl implements StudentService {

	@Override
	public StudentDTO[] getStudents() {
		StudentDTO[] studentDTOs = new StudentDTO[10];
		Connection connection = getConnection();
		Statement stmt;
		try {
			stmt = connection.createStatement();
			ResultSet rs= stmt.executeQuery("select id, fname, email, pan, age from student");
			System.out.println("Row Count = "+rs.getRow());
			int i=0;
			while (rs.next()) {
				int id = rs.getInt(1);
				String firstName = rs.getString(2);
				String email = rs.getString(3);
				String pan = rs.getString(4);
				int age = rs.getInt(5);
				StudentDTO studentDTO = new StudentDTO(id, firstName, email, pan, age);
				studentDTOs[i++] = studentDTO;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return studentDTOs;
	}
	
	@Override
	public StudentDTO getParticularStudent(int studentId) {
		StudentDTO studentDTO = null;
		Connection connection = getConnection();
		Statement stmt;
		try {
			stmt = connection.createStatement();
			ResultSet rs= stmt.executeQuery("select id, fname, email, pan, age from student where id="+studentId);
			
			int columnCount = rs.getMetaData().getColumnCount();
			System.out.print("Column Names-->");
			for(int i=1; i<=columnCount; i++) {
				System.out.print(rs.getMetaData().getColumnLabel(i)+",");
			}
			System.out.println("");
			
			while (rs.next()) {
				int id = rs.getInt(1);
				String firstName = rs.getString(2);
				String email = rs.getString(3);
				String pan = rs.getString(4);
				int age = rs.getInt(5);
				studentDTO = new StudentDTO(id, firstName, email, pan, age);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return studentDTO;
	}
	
	@Override
	public void createStudent(StudentDTO studentDTO) {
		Connection connection = getConnection();
		PreparedStatement pStmt;
		try {
			//pStmt = connection.prepareStatement("insert into students (id, fname,email,pan,age,maths,physics,tamil,chemistry,lname,section,standard) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pStmt = connection.prepareStatement("insert into student (fname,email,pan,age) values (?, ?, ?, ?)");
			pStmt.setString(1, studentDTO.getFirstName());
			pStmt.setString(2, studentDTO.getEmail());
			pStmt.setString(3, studentDTO.getPan());
			pStmt.setInt(4, studentDTO.getAge());
			/*pStmt.setInt(6, 8);
			pStmt.setInt(7, 8);
			pStmt.setInt(8, 8);
			pStmt.setInt(9, 8);
			pStmt.setInt(10, 8);
			pStmt.setInt(11, 8);
			pStmt.setInt(12, 8);*/
			
			boolean insertStatus = pStmt.execute();
			System.out.println("Insertion Status:"+insertStatus);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void particularStudentPut(int studentId, StudentDTO studentDTO) {
		Connection connection = getConnection();
		PreparedStatement pStmt;
		try {
			pStmt = connection.prepareStatement("update student set fname = ?, email=?,pan=?,age=? where id = ?");
			pStmt.setString(1, studentDTO.getFirstName());
			pStmt.setString(2, studentDTO.getEmail());
			pStmt.setString(3, studentDTO.getPan());
			pStmt.setInt(4, studentDTO.getAge());
			pStmt.setInt(5, studentId);
			boolean udpateStatus = pStmt.execute();
			System.out.println("Updation Status:"+udpateStatus);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void particularStudentPatch(int studentId, StudentDTO studentDTO) {
		Connection connection = getConnection();
		PreparedStatement pStmt;
		StudentDTO studentDTOFromDB = null;
		Statement stmt;
		try {
			stmt = connection.createStatement();
			ResultSet rs= stmt.executeQuery("select id, fname, email, pan, age from student where id="+studentId);
			while (rs.next()) {
				int id = rs.getInt(1);
				String firstName = rs.getString(2);
				String email = rs.getString(3);
				String pan = rs.getString(4);
				int age = rs.getInt(5);
				studentDTOFromDB = new StudentDTO(id, firstName, email, pan, age);
			}
			pStmt = connection.prepareStatement("update student set fname = ?, email=?,pan=?,age=? where id = ?");
			pStmt.setString(1, studentDTO.getFirstName() != null ? studentDTO.getFirstName() : studentDTOFromDB.getFirstName());
			pStmt.setString(2, studentDTO.getEmail() != null ? studentDTO.getEmail() : studentDTOFromDB.getEmail() );
			pStmt.setString(3, studentDTO.getPan() != null ? studentDTO.getPan() : studentDTOFromDB.getPan());
			pStmt.setInt(4, studentDTO.getAge() != 0 ? studentDTO.getAge() : studentDTOFromDB.getAge());
			pStmt.setInt(5, studentId);
			boolean udpateStatus = pStmt.execute();
			System.out.println("Updation Status:"+udpateStatus);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void particularStudentDelete(int studentId) {
		Connection connection = getConnection();
		PreparedStatement pStmt;
		try {
			pStmt = connection.prepareStatement("delete from student where id=?");
			pStmt.setInt(1, studentId);
			boolean deleteStatus = pStmt.execute();
			System.out.println("Deletion Status:"+deleteStatus);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private Connection getConnection() {
		Connection conn = null;
		try {
			//Class.forName("com.mysql.cj.jdbc.driver");
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/z_first_schema", "root", "root");
			return conn;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
}
