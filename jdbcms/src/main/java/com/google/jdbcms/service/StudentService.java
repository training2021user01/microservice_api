package com.google.jdbcms.service;

import com.google.jdbcms.dto.StudentDTO;

public interface StudentService {
	
	public StudentDTO[] getStudents();
	
	public StudentDTO getParticularStudent(int studentId);

	public void createStudent(StudentDTO studentDTO);

	public void particularStudentPut(int studentId, StudentDTO studentDTO);
	
	public void particularStudentPatch(int studentId, StudentDTO studentDTO);
	
	public void particularStudentDelete(int studentId);
}
