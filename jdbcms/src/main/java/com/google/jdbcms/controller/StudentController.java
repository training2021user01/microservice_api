package com.google.jdbcms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.jdbcms.dto.StudentDTO;
import com.google.jdbcms.service.StudentService;

@RestController
public class StudentController {
	
	@Autowired
	@Qualifier(value="studentServiceImpl")
	private StudentService studentService;
	
	@GetMapping("/students")
	public ResponseEntity<StudentDTO[]> getStudents() {
		StudentDTO[] studentDTOs = studentService.getStudents();
		return new ResponseEntity<>(studentDTOs, HttpStatus.OK);
	}
	
	@GetMapping("/students/{studentid}")
	public ResponseEntity<StudentDTO> getParticularStudent(@PathVariable("studentid") int studentId) {
		StudentDTO studentDTO = studentService.getParticularStudent(studentId);
		return new ResponseEntity<>(studentDTO, HttpStatus.OK);
	}
	
	/*@GetMapping("/students/")
	public ResponseEntity<StudentDTO> getParticularStudent(@RequestParam("studentid") int studentId) {
		StudentDTO studentDTO = studentService.getParticularStudent(studentId);
		return new ResponseEntity<>(studentDTO, HttpStatus.OK);
	}*/
	
	@PostMapping("/students")
	public ResponseEntity<Void> createStudent(@RequestBody StudentDTO studentDTO) {
		studentService.createStudent(studentDTO);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PutMapping("/students/{studentid}")
	public ResponseEntity<Void> particularStudentPut(@PathVariable("studentid") int studentId, @RequestBody StudentDTO studentDTO) {
		studentService.particularStudentPut(studentId, studentDTO);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PatchMapping("/students/{studentid}")
	public ResponseEntity<Void> particularStudentPatch(@PathVariable("studentid") int studentId, @RequestBody StudentDTO studentDTO) {
		studentService.particularStudentPatch(studentId, studentDTO);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping("/students/{studentid}")
	public ResponseEntity<Void> particularStudentDelete(@PathVariable("studentid") int studentId) {
		studentService.particularStudentDelete(studentId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}


//Webapplication -> webservice -> 1. SOAP (Simple Object Access Protocol) 
							   // 2. REST (Representational State Transfer) -> 1. Microservice
																			// 2. Monolithic