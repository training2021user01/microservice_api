package com.google.jdbcms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class JdbcmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdbcmsApplication.class, args);
	}

}

//DTO -> Data Trasition Object (Controller to Service Layer)
//DAO -> Data Access Object (Service to DB Layer) other names, model or entity

//					  				DTO								 entity/model/dao	
//flow -> Any client -> Contoller ---------> Service -> Service.impl --------------------> DAO/Repository